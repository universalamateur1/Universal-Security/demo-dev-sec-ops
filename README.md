# Flask App explictly Vulnerbale with Secrets

## _Optional_ Commands for local installation and prep

* Git Clone this repo
* virtual enviroment `python3 -m venv .`
* Install reqs `python3 -m  pip install -r requirements.txt`
* run app `python3 -m flask run`

## Artifial vulns

### Werkzeug

* [werkzeug@0.15.1 vulnerabilities](https://security.snyk.io/package/pip/werkzeug/0.15.1) - version 0.15.1

### Flask

* [flask@0.12.2 vulnerabilities](https://security.snyk.io/package/pip/flask/0.12.2) - 0.12.2

### Image

* [python:3.8-slim-buster](https://hub.docker.com/layers/library/python/3.8-slim-buster/images/sha256-4bc25e1810c14af78ca0235ff6f63740113540fd49f8dc5bc6c7b456572b5806?context=explore)

### gitlab-secret-detection-ruleset.toml

* Add file gitlab/secret-detection-ruleset.toml

```bash
.gitlab/secret-detection-ruleset.toml
```

* Add content to file gitlab/secret-detection-ruleset.toml

```yaml
# .gitlab/secret-detection-ruleset.toml
[secrets]
  description = 'secrets custom rules configuration'

  [[secrets.passthrough]]
    type  = "file"
    target = "gitleaks.toml"
    value = ".gitlab/extended-gitleaks-config.toml"
```

### gitlab-extended-gitleaks-config.toml

* Add file gitlab/extended-gitleaks-config.toml

```bash
.gitlab/extended-gitleaks-config.toml
```

* Add content to file gitlab/extended-gitleaks-config.toml

```yaml
# extended-gitleaks-config.toml
title = "extension of gitlab's default gitleaks config"

[extend]
# Extends default packaged path
path = "/gitleaks.toml"

[allowlist]
  description = "allow list of test tokens to ignore in detection"
  regexTarget = "match"
  regexes = ['''glpat-1234567890abcdefghij''',]

# add Test RegEx to the regex table
[[rules]]

# Unique identifier for this rule
id = "test-rule-1"

# Short human readable description of the rule.
description = "Test for Raw Custom Rulesets"

# Golang regular expression used to detect secrets. Note Golang's regex engine
# does not support lookaheads.
regex = '''Custom Raw Ruleset T[est]{3'''

[[rules]]
id = "bearer-auth-token"
description = "Bearer Tokens"
regex = '''`:\s*['" ](?i)bearer\S* \S+`gm'''
```

### Demo RSA private Key

```bash
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAwFR40KI1SzBR0njomkDDEeS0c4r6+7HoUlzqE24hcnP0eJW1
xa2i1dBMkhm56bhWIIq6w6F9Fg8bd5Tsr72OYsiDpoeJ6KV3RVFDv6Ighja+/WdP
4DroQfq0YVo2XoBSPb5uhv46/pNUa5Y+rwVh+SPQlH8sxE7LZODoy8FD2+GskSUs
jk9Y3Cca5/cv7wafh/i3/De5tE5DG1pLlNwbdAG3xCgcgS2806XCMgA5mXt9HXqn
vyuoyyG2gW6sdHb1pwf8rlgoRLbPqwbLpOi245f5XqLmmwBu4Nxc3scseDXfuHDp
Nky1cOzIBePVxktM2DlAAPQ+7Em23TJr0pmrQQIDAQABAoIBAQCnsX9dufDpzAmr
kAyPYmQzV8wW6lkH2AkOt0DJDD9RgdToxvAkmc7eyq3YvWGibT17RjqtlEJyV13F
mC3+1TIu41IWgxs1pAAoikCd+AiPvXAtlkTI59PWo3dfYr8BCrWqbD4GqehaS69R
10B0bicMibO1pmUsDN++53NTJQG71rZp8o3b39Pse3ON/fQLpflPdaBOGkcyXVu5
UB4UDpO8PHtGU3IsxdpM1AVLpari/cYTksgTVczYtYDcTT4Ud2kuB4Q9OmwDTT2K
uecn7S4FR1FS58ILHhC67gWO/A6DnXTUqwiD8/pDY8QJcRQviWoEBQyhMycZSH+O
enzJtCPZAoGBAPPgVlFRMM99r2zSqT0btIG986Pyh3q8dz8ZvfdwUSIbRgqPAG47
ZukRqIVl2d7S9yVtRnFnDteNokG4d5Mk1a3syn87iFKE6wBpr4htWSlOlntVakou
iQq1ngFcA6ABAwtv2eEa1BPaWMuW7q3yWdOYrC36vX1A03llyp4xFNzfAoGBAMnk
IuT1ZRJZpAqd9SveEqW+aYpUrFetYz3JhVuNT49vJoBiq1RrWvgfuEC7gayEDDfb
Gep11XnXPkXspN6415kdarOgiE9CQlADNG9fk0D61O5ONZZTrqGWEBythfoV2xSk
xb6YDPuxs0S6MylQAc9ZRVUpVGLnytHsKjAMVlvfAoGASxFZ4Iv6V1QbxIaPu5Sk
mm8q6ONFmp0ao5y74cd74eC9TZC5FDVKtyFNW0p/ptwPYUDitxN++RDKyioK/IsR
DwldR46+po/temINuxPVpyZeobYoEo+CdX50FX0KTJ0jH8kdKvJEJ5xFSt25uGdq
CPzsuvZ8j2p97ddMaCc5gccCgYAdUI7wh+FBJNr43661y+0RO/C/MURFBtweIKDI
hmBDB3Sjt7AA9gWjeZebbp6JmjLb+Wht7uYsZuCX7qCR5m0Hwom3w1uHhqtySsTW
Vx5elQ1N/PUy+rukotF8GIYXpgzFlpdP8WwRL+BD3nWHTiK1JNU4ZGPoaJe+m3gU
ufXgKQKBgFdbqJfnSrXyrJcDH9nzMwnR6wyFc0RnILie1lrcQZiru9s/Tlv71GJ/
nMEybyOailEeSlBKYy04uVJGPVO4bnzIiGmJdgZlxxjEwHpx/6bkBnh4YVbRAHgc
al8MbhHvfVaRRdRW8eRVuoeHfPge8fRr7UtloYbOEpZh9nTxMUHj
-----END RSA PRIVATE KEY-----
```

## Resources

* [Docu Push rules](https://docs.gitlab.com/ee/user/project/repository/push_rules.html#prevent-pushing-secrets-to-the-repository)
* [Files deny list for push rules](https://gitlab.com/gitlab-org/gitlab/-/blob/master/ee/lib/gitlab/checks/files_denylist.yml)
* [Secuirty Policies in docs](https://docs.gitlab.com/ee/user/application_security/policies/#security-policy-project)
* [Secret Dteection in docs](https://docs.gitlab.com/ee/user/application_security/secret_detection/)
* [Saved regex](https://regex101.com/r/PGscpO/1)

### RegEx Examplesa

```python
regex = '`:\s*['" ](?i)bearer\S* \S+`gm'
headers = {"Authorization": "Bearer MYREALLYLONGTOKENIGOT"}
metaData= {'authorization' : "bearer jdewRVKNL3AQ52AWSZwQ9WddDbOvHtQuW2O",
WWW-Authenticate: Bearer realm="Keksfabrik"
Authorization: Bearer S0VLU0UhIExFQ0tFUiEK
curl -H "Authorization: bearer MYREALLYLONGTOKENIhad" http://www.example.com
```

### CI Yaml File for Secret detection

```yaml
# You can override the included template(s) by including variable overrides
# SAST customization: https://docs.gitlab.com/ee/user/application_security/sast/#customizing-the-sast-settings
# Secret Detection customization: https://docs.gitlab.com/ee/user/application_security/secret_detection/#customizing-settings
# Dependency Scanning customization: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#customizing-the-dependency-scanning-settings
# Container Scanning customization: https://docs.gitlab.com/ee/user/application_security/container_scanning/#customizing-the-container-scanning-settings
# Note that environment variables can be set in several places
# See https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence
include:
- template: Security/Secret-Detection.gitlab-ci.yml

# https://docs.gitlab.com/ee/user/application_security/secret_detection/#configure-scan-settings
secret_detection:
  variables:
    SECRET_DETECTION_EXCLUDED_PATHS: "test, README.md" # Exclude the content of the test folder and README.md
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      variables:                              # Override SECRET_DETECTION_HISTORIC_SCAN
        SECRET_DETECTION_HISTORIC_SCAN: "true"  # at the job level.
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH
      variables:
        SECRET_DETECTION_HISTORIC_SCAN: "false" # On not default branch ignore History

# Better Logs for your Job Logs test
variables:
  SECURE_LOG_LEVEL: debug
```

Test Push 2

# Python App
